package apierrors

import (
	"fmt"
	"net/http"
)

type APIError struct {
	HTTPStatus  int    `json:"-"`
	Status      int    `json:"code,omitempty"`
	Message     string `json:"message,omitempty"`
	Description string `json:"description,omitempty"`
}

func (ae APIError) WithDescription(description string) APIError {
	ae.Description = description
	return ae
}

func (ae APIError) Error() string {
	return fmt.Sprintf("error code %d: %s: %s", ae.Status, ae.Message, ae.Description)
}

// noinspection GoUnusedGlobalVariable
var (
	Internal = APIError{
		HTTPStatus: http.StatusInternalServerError,
		Status:     500,
		Message:    "Internal Server Error",
	}
	Timeout = APIError{
		HTTPStatus: http.StatusGatewayTimeout,
		Status:     504,
		Message:    "Gateway Timeout",
	}
	NotFound = APIError{
		HTTPStatus: http.StatusNotFound,
		Status:     404,
		Message:    "Not Found",
	}
	NotFoundToken       = NotFound.WithDescription("token not exist")
	NotFoundPair        = NotFound.WithDescription("pair not exist")
	NotFoundCollection  = NotFound.WithDescription("collection not exist")
	NotFoundAsset       = NotFound.WithDescription("asset not exist")
	NotFoundFarm        = NotFound.WithDescription("farm not exist")
	NotFoundFarmFactory = NotFound.WithDescription("farm factory not exist")

	ServiceUnavailable = APIError{
		HTTPStatus: http.StatusServiceUnavailable,
		Status:     503,
		Message:    "Service Unavailable",
	}
	RequestTimeout = APIError{
		HTTPStatus: http.StatusRequestTimeout,
		Status:     504,
		Message:    "Request Timeout",
	}
	TooManyRequests = APIError{
		HTTPStatus: http.StatusTooManyRequests,
		Status:     429,
		Message:    "Too many requests",
	}
)

// noinspection GoUnusedGlobalVariable
var (
	ResponseFormatterError = Internal.WithDescription("response formatter error")

	AuthBadFormat = APIError{
		HTTPStatus: http.StatusUnauthorized,
		Status:     1004,
		Message:    "Unsupported authorization method",
	}
	AuthForbidden = APIError{
		HTTPStatus: http.StatusForbidden,
		Status:     1003,
		Message:    "Action is forbidden for this API key",
	}
	AuthUnauthorized = APIError{
		HTTPStatus: http.StatusUnauthorized,
		Status:     1002,
		Message:    "Authorization is required or has been failed",
	}

	Validation = APIError{
		HTTPStatus: http.StatusBadRequest,
		Status:     10001,
		Message:    "Validation error",
	}
	IncorrectAddress = Validation.WithDescription("incorrect address")
	IncorrectID      = Validation.WithDescription("incorrect ID")
	InvalidRequest   = Validation.WithDescription("cannot parse request")

	DuplicateRequest = APIError{
		HTTPStatus: http.StatusBadRequest,
		Status:     20043,
		Message:    "Duplicate request",
	}

	NotAllowed = APIError{
		HTTPStatus: http.StatusBadRequest,
		Status:     600,
		Message:    "Action not allowed",
	}
)
