package memory

import (
	"errors"
	"sync"
	"time"
)

const (
	Permanent = -1
)

var (
	ErrKeyNotFound = errors.New("key not found")
)

type Storage struct {
	items             map[string]Item
	itemsMtx          sync.RWMutex
	expirationTimeout time.Duration
	cleanupTimeout    time.Duration
}

type Item struct {
	Value      interface{}
	Expiration int64
}

func New(expiration, cleanup time.Duration) *Storage {
	cache := Storage{
		items:             make(map[string]Item),
		expirationTimeout: expiration,
		cleanupTimeout:    cleanup,
	}

	if cache.cleanupTimeout > 0 {
		go cache.startGC()
	}
	return &cache
}

func (c *Storage) Set(key string, value interface{}) {
	var item = Item{Value: value}
	if c.expirationTimeout > 0 {
		item.Expiration = time.Now().Add(c.expirationTimeout).UnixNano()
	}
	c.itemsMtx.Lock()
	c.items[key] = item
	c.itemsMtx.Unlock()
}

func (c *Storage) SetTtl(key string, value interface{}, duration time.Duration) {
	var expiration int64
	if duration == 0 {
		duration = c.expirationTimeout
	}

	if duration > 0 {
		expiration = time.Now().Add(duration).UnixNano()
	}

	c.itemsMtx.Lock()
	c.items[key] = Item{
		Value:      value,
		Expiration: expiration,
	}
	c.itemsMtx.Unlock()
}

func (c *Storage) Get(key string) (interface{}, bool) {
	c.itemsMtx.RLock()

	item, found := c.items[key]
	if !found {
		c.itemsMtx.RUnlock()
		return nil, false
	}

	if item.Expiration > 0 && time.Now().UnixNano() > item.Expiration {
		c.itemsMtx.RUnlock()
		return nil, false
	}

	c.itemsMtx.RUnlock()
	return item.Value, true
}

func (c *Storage) Delete(key string) error {
	c.itemsMtx.Lock()
	if _, ok := c.items[key]; !ok {
		c.itemsMtx.Unlock()
		return ErrKeyNotFound
	}
	delete(c.items, key)
	c.itemsMtx.Unlock()
	return nil
}

func (c *Storage) DeleteAll() {
	c.itemsMtx.Lock()
	for k := range c.items {
		delete(c.items, k)
	}
	c.itemsMtx.Unlock()
}

func (c *Storage) startGC() {
	var ticker = time.NewTicker(c.cleanupTimeout)
	defer ticker.Stop()

	for range ticker.C {
		if len(c.items) == 0 {
			continue
		}

		if keys := c.expiredKeys(); len(keys) > 0 {
			c.clearItems(keys)
		}
	}
}

func (c *Storage) expiredKeys() (keys []string) {
	c.itemsMtx.RLock()
	for k, i := range c.items {
		if i.Expiration > 0 && time.Now().UnixNano() > i.Expiration {
			keys = append(keys, k)
		}
	}
	c.itemsMtx.RUnlock()
	return
}

func (c *Storage) clearItems(keys []string) {
	c.itemsMtx.Lock()
	for i := range keys {
		delete(c.items, keys[i])
	}
	c.itemsMtx.Unlock()
}
