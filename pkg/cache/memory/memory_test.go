package memory_test

import (
	"testing"
	"time"

	"lab.aviproduction.org/aggregator/go-kit/pkg/cache/memory"

	"github.com/stretchr/testify/assert"
)

func TestStorage_Cache(t *testing.T) {
	t.Parallel()

	type testCase struct {
		name      string
		key       string
		value     string
		sleep     time.Duration
		available bool
	}
	var tests = []testCase{
		{
			name:      "empty value",
			key:       "key0",
			available: false,
		},
		{
			name:      "set and get value",
			value:     "value",
			key:       "kye1",
			available: true,
		},
		{
			name:      "expired key",
			value:     "value",
			key:       "key2",
			sleep:     time.Second,
			available: false,
		},
	}

	storage := memory.New(time.Second, time.Minute)
	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			if len(test.value) > 0 {
				storage.Set(test.key, test.value)
			}
			if test.sleep > 0 {
				time.Sleep(test.sleep)
			}
			content, ok := storage.Get(test.key)
			assert.EqualValues(t, test.available, ok)
			if len(test.value) > 0 && test.available {
				assert.EqualValues(t, test.value, content.(string))
			}
		})
	}
}
