package shutdown

import "context"

type Shutdown struct {
	ctx    context.Context
	cancel context.CancelFunc
}

func New(ctx context.Context) Shutdown {
	ctxShutdown, cancel := context.WithCancel(ctx)
	return Shutdown{ctx: ctxShutdown, cancel: cancel}
}

func (s *Shutdown) Stop() {
	s.cancel()
}

func (s *Shutdown) Ctx() context.Context {
	return s.ctx
}
