package config

import (
	"fmt"
	"time"

	"github.com/powerman/structlog"
	"github.com/spf13/viper"
)

type Config struct {
	Service    Service
	Database   Database
	Prometheus Prometheus
	Rest       Rest
	Nats       Nats
	Clusters   []Cluster
}

type Cluster struct {
	ID   uint32 `yaml:"id"`
	Name string `yaml:"name"`
}

type Database struct {
	Host               string `yaml:"host"`
	Port               int    `yaml:"port"`
	Name               string `yaml:"name"`
	User               string `yaml:"user"`
	Pass               string `yaml:"pass"`
	MaxIdleConnections int    `yaml:"maxIdleConnections"`
	MaxOpenConnections int    `yaml:"maxOpenConnections"`
	MigrationPath      string `yaml:"migrationPath"`
}

func (c *Database) DSN() string {
	return fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		c.Host, c.Port, c.User, c.Pass, c.Name,
	)
}

func (c *Database) IsValid() bool {
	return len(c.Host) > 0 && len(c.Name) > 0 && len(c.User) > 0 && len(c.Pass) > 0 && c.Port > 0
}

type Service struct {
	GracePeriod time.Duration `yaml:"gracePeriod"`
	MaxCpuUsage int           `yaml:"maxCpuUsage"`
}

type Prometheus struct {
	Host   string `yaml:"host"`
	Port   int    `yaml:"port"`
	Enable bool   `yaml:"enable"`
}

func (p Prometheus) Addr() string {
	return fmt.Sprintf("%s:%d", p.Host, p.Port)
}

type Rest struct {
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
}

func (r Rest) Addr() string {
	return fmt.Sprintf("%s:%d", r.Host, r.Port)
}

type Nats struct {
	Proto string `yaml:"proto" required:"true" default:"nats"`
	Host  string `yaml:"host" required:"true" default:"localhost"`
	Port  string `yaml:"port" required:"true" default:"4222"`

	AllowReconnect bool          `yaml:"allowReconnect" split_words:"true" required:"true" default:"true"`
	MaxReconnect   int           `yaml:"maxReconnect" split_words:"true" required:"true" default:"10"`
	ReconnectWait  time.Duration `yaml:"reconnectWait" split_words:"true" required:"true" default:"100ms"`
	Timeout        time.Duration `yaml:"timeout" required:"true" default:"1s"`
	PingInterval   time.Duration `yaml:"pingInterval" split_words:"true" required:"true" default:"20s"`
	MaxPingsOut    int           `yaml:"MaxPingsOut" split_words:"true" required:"true" default:"5"`
	GracePeriod    time.Duration `yaml:"gracePeriod" split_words:"true" required:"true" default:"40s"`

	SubscriptionWorkers int `yaml:"subscriptionWorkers" split_words:"true" required:"true" default:"5"`
}

func (n Nats) DSN() string {
	return n.Proto + "://" + n.Host + ":" + n.Port
}

func InitConfiguration(logger *structlog.Logger) Config {
	var err error
	cfg := Config{}

	viper.SetConfigFile("config.yaml")
	err = viper.ReadInConfig()
	if err != nil {
		logger.Fatalln("fatal load config file", err)
	}

	err = viper.UnmarshalKey("config", &cfg)
	if err != nil {
		logger.Fatalln("init configuration", "failed to unmarshal config", err.Error())
	}

	if cfg.Service.GracePeriod == 0 {
		cfg.Service.GracePeriod = time.Minute
	}

	return cfg
}
