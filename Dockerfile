# docker build --no-cache -t commision-manager:latest -f Dockerfile .

# Accept the Go version for the image to be set as a build argument.
# Default to Go 1.11
ARG GO_VERSION=1.19.9

# First stage: build the executable.
FROM golang:${GO_VERSION} AS builder

ARG GITLAB_ACCESS_USER="gitlab-ci-token"
ARG GITLAB_ACCESS_TOKEN

# Set the environment variables for the go command:
# * CGO_ENABLED=0 to build a statically-linked executable
ENV CGO_ENABLED=0

# Set the working directory outside $GOPATH to enable the support for modules.
WORKDIR /build

RUN git config --global --add url."https://${GITLAB_ACCESS_USER}:${GITLAB_ACCESS_TOKEN}@gitlab.com/serkurnikov/amazon/".insteadOf "https://gitlab.com/serkurnikov/amazon/"

# Import the code from the context.
COPY go.mod       ./go.mod
COPY go.sum       ./go.sum
COPY cmd/         ./cmd
COPY config/      ./config
COPY internal     ./internal
COPY pkg          ./pkg
COPY migrations   ./migrations
COPY config.yaml  ./config.yaml

RUN go mod download && go mod verify

# Build binary file.
RUN go build -o /build/amazon ./cmd/amazon

# Final stage: the running container.
FROM debian:11 AS final

# Import the compiled executable from the second stage.
COPY --from=builder /build/amazon /amazon

# Import migrations
COPY --from=builder /build/migrations /migrations
# Import configs
COPY --from=builder /build/config.yaml /config.yaml

CMD ["/amazon"]

