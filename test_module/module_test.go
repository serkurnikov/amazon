package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"testing"

	"gitlab.com/serkurnikov/amazon/api/openapi/model"
)

func TestUploadAndFetchFile(t *testing.T) {
	// Create a new HTTP request for uploading a file
	uploadURL := "http://127.0.0.1:8080/api/v1/upload"
	fileContent, err := os.ReadFile("test.jpg")
	if err != nil {
		t.Fatal(err)
	}

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", "test.jpg")
	if err != nil {
		t.Fatal(err)
	}
	part.Write(fileContent)
	writer.Close()

	req, err := http.NewRequest("POST", uploadURL, body)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", writer.FormDataContentType())

	// Perform the HTTP request
	client := &http.Client{}
	uploadResp, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	defer uploadResp.Body.Close()

	// Check the HTTP response status code and body
	if uploadResp.StatusCode != http.StatusOK {
		t.Fatalf("Expected status code %d, got %d", http.StatusOK, uploadResp.StatusCode)
	}

	uploadResponseBody, err := io.ReadAll(uploadResp.Body)
	if err != nil {
		t.Fatal(err)
	}

	// Parse the response JSON to get the file ID
	var fileID struct {
		ID uint32 `json:"id"`
	}
	if err := json.Unmarshal(uploadResponseBody, &fileID); err != nil {
		t.Fatal(err)
	}

	// Use the obtained file ID to fetch the file
	fetchURL := fmt.Sprintf("http://127.0.0.1:8080/api/v1/get?id=%d", fileID.ID)
	fetchResp, err := http.Get(fetchURL)
	if err != nil {
		t.Fatal(err)
	}
	defer fetchResp.Body.Close()

	// Check the HTTP response status code and body
	if fetchResp.StatusCode != http.StatusOK {
		t.Fatalf("Expected status code %d, got %d", http.StatusOK, fetchResp.StatusCode)
	}

	// Parse the fetched JSON response into a FileData struct
	var fetchedFile model.FileData
	if err := json.NewDecoder(fetchResp.Body).Decode(&fetchedFile); err != nil {
		t.Fatal(err)
	}

	// Decode the Base64 content
	decodedContent, err := base64.StdEncoding.DecodeString(*fetchedFile.Content)
	if err != nil {
		t.Fatal(err)
	}

	// Compare the decoded content with the original file content
	if !bytes.Equal(fileContent, decodedContent) {
		t.Fatal("Fetched file content does not match the original file content")
	}
}
