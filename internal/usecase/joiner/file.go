package joiner

import (
	"context"
	"encoding/base64"
	"fmt"
	"sort"
	"sync"

	"github.com/AlekSi/pointer"
	"github.com/google/uuid"
	"gitlab.com/serkurnikov/amazon/api/openapi/model"
	"gitlab.com/serkurnikov/amazon/internal/domain"
)

func (s *Service) RequestChunksFromServers(ctx context.Context, fileID uint32) (*model.FileData, error) {
	s.logger.Info("Request", "fileID", fileID)
	requestID := uuid.New().String()

	total, err := s.repo.GetChunksCountByFileID(ctx, fileID)
	if err != nil {
		return nil, err
	}

	// Create a new wait group for this request
	var wg sync.WaitGroup
	wg.Add(int(total))

	// Store the wait group in the map using the request ID as the key
	s.chunkWaitGroups.Store(requestID, &wg)

	var chunks []domain.Chunk
	for _, publisher := range s.publishers {
		err := publisher.FetchFile(ctx, domain.ChunkGetEvent{
			RequestID: requestID,
			FileID:    fileID,
		})
		if err != nil {
			// You can choose whether to continue or return an error here, depending on your requirements
			s.logger.PrintErr("failed to fetch part file via NATS", "error", err)
			wg.Done() // Decrement the wait group counter in case of an error
			return nil, err
		}
	}

	// Wait for all chunks to be received for this request
	wg.Wait()

	// Combine the received chunks into a complete file
	cacheFileKey := fmt.Sprintf("%s%v", requestID, fileID)
	if cached, ok := s.cache.Get(cacheFileKey); ok {
		data := cached.(domain.File)
		chunks = append(chunks, data.Chunks...)
	}

	fileContent := CombineChunks(chunks)
	// Create a base64-encoded string from the file content
	encodedContent := base64.StdEncoding.EncodeToString(fileContent)

	// Remove the wait group from the map after the request is completed
	s.chunkWaitGroups.Delete(requestID)

	fileName, err := s.repo.GetFileNameByID(ctx, fileID)
	if err != nil {
		return nil, err
	}

	s.logger.Info("file is completed", "fileID", fileID, "chunks", len(chunks))
	return &model.FileData{
		Content: &encodedContent,
		ID:      pointer.ToInt32(int32(fileID)),
		Name:    pointer.ToString(fileName),
	}, nil
}

func (s *Service) JoinChunk(ctx context.Context, chunk domain.ChunkSaveEvent) error {
	// Log that a chunk has been received
	s.logger.Info("Received chunk", "serialID", chunk.SerialNumberID)

	// Get the wait group associated with this request ID
	if wg, ok := s.chunkWaitGroups.Load(chunk.RequestID); ok {
		// Signal the wait group that a chunk has been received for this request
		wg.(*sync.WaitGroup).Done()
	}

	// Update the cache with the received chunk
	cacheFileKey := fmt.Sprintf("%s%v", chunk.RequestID, chunk.FileID)
	cachedData, ok := s.cache.Get(cacheFileKey)
	var chunks []domain.Chunk

	if ok {
		// If data is already in the cache, convert it into a chunk slice
		cachedFile := cachedData.(domain.File)
		chunks = cachedFile.Chunks
	} else {
		// If data is not in the cache, create a new slice for chunks
		chunks = make([]domain.Chunk, 0)
	}

	// Add the new chunk to the slice
	chunks = append(chunks, domain.Chunk{
		FileID:         chunk.FileID,
		SerialNumberID: chunk.SerialNumberID,
		Data:           chunk.Data,
	})

	// Update the data in the cache
	s.cache.Set(cacheFileKey, domain.File{
		ID:     chunk.FileID,
		Chunks: chunks,
	})
	return nil
}

// CombineChunks combines chunks into a complete file.
func CombineChunks(chunks []domain.Chunk) []byte {
	// Sort the chunks by SerialNumberID to ensure they are in the correct order
	sort.Slice(chunks, func(i, j int) bool {
		return chunks[i].SerialNumberID < chunks[j].SerialNumberID
	})

	// Calculate the total size of the complete file
	totalSize := 0
	for _, chunk := range chunks {
		totalSize += len(chunk.Data)
	}

	// Create a buffer to hold the complete file data
	completeFile := make([]byte, totalSize)

	// Copy the data from each chunk into the complete file buffer
	offset := 0
	for _, chunk := range chunks {
		copy(completeFile[offset:], chunk.Data)
		offset += len(chunk.Data)
	}
	return completeFile
}
