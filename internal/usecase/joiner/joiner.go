package joiner

import (
	"context"
	"sync"
	"time"

	"github.com/powerman/structlog"
	"gitlab.com/serkurnikov/amazon/api/openapi/model"
	"gitlab.com/serkurnikov/amazon/config"
	"gitlab.com/serkurnikov/amazon/internal/domain"
	"gitlab.com/serkurnikov/amazon/internal/provider/nats"
	"gitlab.com/serkurnikov/amazon/pkg/cache/memory"
	"gitlab.com/serkurnikov/amazon/pkg/shutdown"
)

type Usecase interface {
	RequestChunksFromServers(ctx context.Context, fileID uint32) (*model.FileData, error)
	JoinChunk(ctx context.Context, chunk domain.ChunkSaveEvent) error

	Start() error
	Stop()
}

type Repo interface {
	GetChunksCountByFileID(ctx context.Context, fileID uint32) (uint32, error)
	GetFileNameByID(ctx context.Context, fileID uint32) (string, error)
}

type Service struct {
	chunkWaitGroups sync.Map
	shutdown        shutdown.Shutdown

	repo       Repo
	cache      *memory.Storage
	config     *config.Config
	logger     *structlog.Logger
	publishers []*nats.Publisher
}

func NewService(ctxShutdown context.Context, repo Repo, config *config.Config, publishers []*nats.Publisher, logger *structlog.Logger) Usecase {
	return &Service{
		repo:       repo,
		cache:      memory.New(5*time.Minute, 20*time.Minute),
		shutdown:   shutdown.New(ctxShutdown),
		config:     config,
		publishers: publishers,
		logger:     logger,
	}
}

func (s *Service) Start() error {
	return nil
}

func (s *Service) Stop() {
	s.shutdown.Stop()
}
