package splitter

import (
	"context"
	"fmt"
	"io"
	"mime/multipart"
	"time"

	"github.com/cenkalti/backoff/v4"
	"gitlab.com/serkurnikov/amazon/internal/domain"
)

const (
	maxPartSize = 0.5 * 1024 * 1024 // 0.5 MB

	maxElapsedTime       = 5 * time.Second
	initialRetryInterval = 5 * time.Second
	maxRetryInterval     = 10 * time.Second
)

func (s *Service) SplitFileAndSendToServers(ctx context.Context, file multipart.File, header *multipart.FileHeader) (uint32, error) {
	defer file.Close()

	fileDatabaseID, err := s.repo.CreateFile(ctx, &domain.File{
		Name: header.Filename,
		Size: header.Size,
	})
	if err != nil {
		return 0, err
	}

	var totalChunks uint32
	// Calculate the total number of clusters (publishers)
	totalClusters := len(s.publishers)
	if totalClusters == 0 {
		return 0, nil // No clusters (publishers), nothing to do
	}

	buffer := make([]byte, maxPartSize)

	for i := 0; ; i++ {
		// Read a part from the file, limited to maxPartSize
		n, err := file.Read(buffer)
		if err == io.EOF {
			break
		}
		if err != nil {
			s.logger.PrintErr("failed to read file part", "error", err)
			return 0, err
		}

		// Send the part to the corresponding server using the publisher
		publisher := s.publishers[i%totalClusters]
		err = publisher.UploadFile(ctx, domain.ChunkSaveEvent{
			FileID:         fileDatabaseID,
			SerialNumberID: uint32(i),
			Data:           buffer[:n],
		})
		if err != nil {
			// You can choose whether to continue or return an error here, depending on your requirements
			s.logger.PrintErr("failed to send part file via NATS", "error", err)
			return 0, err
		}

		s.logger.Info("sending part file to", "clusterID", publisher.ClusterID)
		totalChunks++
	}

	err = s.repo.UpdateFileChunksCount(ctx, fileDatabaseID, totalChunks)
	if err != nil {
		s.logger.PrintErr("failed to update chunks count", "err", err.Error())
		return 0, err
	}

	s.logger.Info("file sent in parts", "totalClusters", totalClusters)

	// check that all part saved in servers successfully
	err = s.checkChunksSavedSuccessfully(ctx, fileDatabaseID, totalChunks)
	if err != nil {
		// remove all files from database
		return 0, err
	}

	return fileDatabaseID, nil
}

func (s *Service) checkChunksSavedSuccessfully(ctx context.Context, fileID, expectedCount uint32) error {
	// Define the retry strategy
	retryStrategy := backoff.NewExponentialBackOff()
	retryStrategy.MaxElapsedTime = maxElapsedTime
	retryStrategy.InitialInterval = initialRetryInterval
	retryStrategy.MaxInterval = maxRetryInterval

	// Define the number of retries per attempt
	maxRetries := 3

	// Outer loop to repeat the entire operation
	for attempt := 0; attempt < maxRetries; attempt++ {
		operation := func() error {
			// Checking info in the database
			totalCount, err := s.repo.GetChunksCountByFileID(ctx, fileID)
			if err == nil {
				if totalCount == expectedCount {
					return nil // Success, no need to retry
				}
				return fmt.Errorf("not match chunks total count")
			}
			// Wait for the next retry
			time.Sleep(retryStrategy.NextBackOff())
			return fmt.Errorf("max retries reached") // Return an error if max retries reached
		}

		// Retry the operation using the defined strategy
		err := backoff.Retry(operation, backoff.WithMaxRetries(retryStrategy, uint64(maxRetries)))
		if err == nil {
			return nil // Success, no need to repeat the operation
		}
		// Sleep before the next attempt
		time.Sleep(retryStrategy.NextBackOff())
	}

	return fmt.Errorf("max attempts reached") // Return an error if max attempts reached
}
