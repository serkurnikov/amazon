package splitter

import (
	"context"
	"mime/multipart"

	"github.com/powerman/structlog"
	"gitlab.com/serkurnikov/amazon/config"
	"gitlab.com/serkurnikov/amazon/internal/domain"
	"gitlab.com/serkurnikov/amazon/internal/provider/nats"
	"gitlab.com/serkurnikov/amazon/pkg/shutdown"
)

type Usecase interface {
	SplitFileAndSendToServers(ctx context.Context, file multipart.File, header *multipart.FileHeader) (uint32, error)

	Start() error
	Stop()
}

type Repo interface {
	CreateFile(ctx context.Context, f *domain.File) (uint32, error)

	UpdateFileChunksCount(ctx context.Context, fileID, count uint32) error
	GetChunksCountByFileID(ctx context.Context, fileID uint32) (uint32, error)
}

type Splitter interface {
}

type Service struct {
	shutdown shutdown.Shutdown

	repo       Repo
	config     *config.Config
	logger     *structlog.Logger
	publishers []*nats.Publisher
}

func NewService(ctxShutdown context.Context, repo Repo, config *config.Config, publishers []*nats.Publisher, logger *structlog.Logger) Usecase {
	return &Service{
		repo:       repo,
		shutdown:   shutdown.New(ctxShutdown),
		config:     config,
		publishers: publishers,
		logger:     logger,
	}
}

func (s *Service) Start() error {
	return nil
}

func (s *Service) Stop() {
	s.shutdown.Stop()
}
