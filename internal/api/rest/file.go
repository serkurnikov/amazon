package rest

import (
	"github.com/AlekSi/pointer"
	"github.com/pkg/errors"
	"gitlab.com/serkurnikov/amazon/api/openapi/model"
	"gitlab.com/serkurnikov/amazon/api/openapi/restapi/op"
	"gitlab.com/serkurnikov/amazon/pkg/apierrors"
)

func (h *Handler) getFile(params op.GetFileParams) op.GetFileResponder {
	ctx, remoteIP, log := fromRequest(params.HTTPRequest)
	if err := h.limiter.checkVisit(ctx, remoteIP); err != nil {
		return errGetFile(log, apierrors.TooManyRequests)
	}

	fileData, err := h.us.Joiner.RequestChunksFromServers(ctx, uint32(params.ID))
	switch errors.Cause(err) {
	default:
		h.logger.PrintErr(err)
		return errGetFile(log, err)
	case nil:
	}

	return op.NewGetFileOK().WithPayload(fileData)
}

func (h *Handler) uploadFile(params op.UploadFileParams) op.UploadFileResponder {
	ctx, remoteIP, log := fromRequest(params.HTTPRequest)
	if err := h.limiter.checkVisit(ctx, remoteIP); err != nil {
		return errUploadFile(log, apierrors.TooManyRequests)
	}

	file, handler, err := params.HTTPRequest.FormFile("file")
	if err != nil {
		return errUploadFile(log, apierrors.Internal)
	}

	fileID, err := h.us.Splitter.SplitFileAndSendToServers(ctx, file, handler)
	switch errors.Cause(err) {
	default:
		h.logger.PrintErr(err)
		return errUploadFile(log, err)
	case nil:
	}
	return op.NewUploadFileOK().WithPayload(&model.UploadFileResponse{
		ID: pointer.ToInt32(int32(fileID)),
	})
}
