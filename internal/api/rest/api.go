package rest

import (
	"context"
	"net"
	"net/http"
	"path"
	"strconv"
	"strings"
	"time"

	"github.com/felixge/httpsnoop"
	"github.com/go-openapi/loads"
	"github.com/powerman/structlog"
	"github.com/prometheus/client_golang/prometheus"
	corspkg "github.com/rs/cors"
	"github.com/sebest/xff"
	"gitlab.com/serkurnikov/amazon/api/openapi/restapi"
	"gitlab.com/serkurnikov/amazon/api/openapi/restapi/op"
	"gitlab.com/serkurnikov/amazon/config"
	"gitlab.com/serkurnikov/amazon/internal/services/metrics"
	"gitlab.com/serkurnikov/amazon/pkg/def"
)

type (
	Ctx = context.Context
	Log = *structlog.Logger

	Config struct {
		Host string
		Port int
	}
)

func NewServer(cfg config.Rest, log *structlog.Logger, metricSvc *metrics.Metric, us Usecases) (*restapi.Server, error) {
	swaggerSpec, err := loads.Embedded(restapi.SwaggerJSON, restapi.FlatSwaggerJSON)
	if err != nil {
		return nil, err
	}

	api := op.NewAmazonAPIAPI(swaggerSpec)
	api.Logger = log.Printf

	handler := NewHandler(us, log)
	api.HealthCheckHandler = op.HealthCheckHandlerFunc(handler.healthCheck)
	api.UploadFileHandler = op.UploadFileHandlerFunc(handler.uploadFile)
	api.GetFileHandler = op.GetFileHandlerFunc(handler.getFile)

	server := restapi.NewServer(api)
	server.EnabledListeners = []string{"http"}
	server.Port = cfg.Port
	server.Host = cfg.Host
	server.ReadTimeout = 30 * time.Second
	server.WriteTimeout = 30 * time.Second

	// The middleware executes before anything.
	globalMiddlewares := func(handler http.Handler) http.Handler {
		xffmw, _ := xff.Default()
		logger := makeLogger(swaggerSpec.BasePath())
		accessLog := makeAccessLog(swaggerSpec.BasePath(), metricSvc)
		return noCache(xffmw.Handler(logger(recovery(accessLog(disableSwaggerURL(swaggerSpec.BasePath(), cors(handler)))))))
	}
	// The middleware executes after serving /swagger.json and routing,
	// but before authentication, binding and validation.
	middlewares := func(handler http.Handler) http.Handler {
		return handler
	}

	server.SetHandler(globalMiddlewares(api.Serve(middlewares)))
	return server, nil
}

type middlewareFunc func(http.Handler) http.Handler

func makeLogger(basePath string) middlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			remote := xff.GetRemoteAddr(r)
			log := structlog.New(
				structlog.KeyTime, structlog.Auto,
				def.LogRemote, remote,
				def.LogHTTPStatus, "",
				def.LogHTTPMethod, r.Method,
				def.LogFunc, strings.TrimPrefix(r.URL.Path, basePath),
			).SetTimeFormat(time.StampMicro)

			r = r.WithContext(structlog.NewContext(r.Context(), log))

			next.ServeHTTP(w, r)
		})
	}
}

func makeAccessLog(basePath string, metricSvc *metrics.Metric) middlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			metricSvc.ReqInFlight.Inc()
			defer metricSvc.ReqInFlight.Dec()

			m := httpsnoop.CaptureMetrics(next, w, r)

			l := prometheus.Labels{
				metrics.ResourceLabel: strings.TrimPrefix(r.URL.Path, basePath),
				metrics.MethodLabel:   r.Method,
				metrics.CodeLabel:     strconv.Itoa(m.Code),
			}
			metricSvc.ReqTotal.With(l).Inc()

			l = prometheus.Labels{
				metrics.ResourceLabel: strings.TrimPrefix(r.URL.Path, basePath),
				metrics.MethodLabel:   r.Method,
				metrics.FailedLabel:   strconv.FormatBool(m.Code >= http.StatusInternalServerError),
			}
			metricSvc.ReqDuration.With(l).Observe(m.Duration.Seconds())

			log := structlog.FromContext(r.Context(), nil)
			if m.Code > http.StatusOK && m.Code < http.StatusInternalServerError {
				log.Info("handled", def.LogHTTPStatus, m.Code, "URL", r.URL.Path)
			} else if m.Code >= http.StatusInternalServerError {
				log.PrintErr("failed to handle", def.LogHTTPStatus, m.Code, "URL", r.URL.Path)
			}
		})
	}
}

func noCache(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Expires", "0")
		w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
		w.Header().Set("Pragma", "no-cache")
		w.Header().Set("X-XSS-Protection", "1; mode=block")
		w.Header().Set("X-Frame-Options", "SAMEORIGIN")
		w.Header().Set("X-Content-Type-Options", "nosniff")
		w.Header().Set("Strict-Transport-Security", "max-age=15552000; includeSubDomains")
		next.ServeHTTP(w, r)
	})
}

func recovery(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			switch err := recover(); err := err.(type) {
			default:
				log := structlog.FromContext(r.Context(), nil)
				log.PrintErr(err, structlog.KeyStack, structlog.Auto)
				w.WriteHeader(http.StatusInternalServerError)
			case nil:
			case net.Error:
				log := structlog.FromContext(r.Context(), nil)
				log.PrintErr(err)
				w.WriteHeader(http.StatusInternalServerError)
			}
		}()

		next.ServeHTTP(w, r)
	})
}

func disableSwaggerURL(basePath string, next http.Handler) http.Handler {
	swaggerPath := path.Join(basePath, "swagger.json")
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == swaggerPath {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func cors(next http.Handler) http.Handler {
	return corspkg.AllowAll().Handler(next)
}
