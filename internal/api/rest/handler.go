package rest

import (
	"context"

	"net"
	"net/http"

	"github.com/powerman/structlog"
	"gitlab.com/serkurnikov/amazon/api/openapi/restapi/op"
	"gitlab.com/serkurnikov/amazon/internal/usecase/joiner"
	"gitlab.com/serkurnikov/amazon/internal/usecase/splitter"
	"gitlab.com/serkurnikov/amazon/pkg/def"
)

const (
	OneTimeBurst = 8
	LimitPerSec  = 2
)

type Usecases struct {
	Splitter splitter.Usecase
	Joiner   joiner.Usecase
}

type Handler struct {
	us      Usecases
	limiter *limiter
	logger  *structlog.Logger
}

func NewHandler(us Usecases, logger *structlog.Logger) *Handler {
	return &Handler{
		us:      us,
		limiter: newLimiter(LimitPerSec, OneTimeBurst),
		logger:  logger,
	}
}

func fromRequest(r *http.Request) (context.Context, string, *structlog.Logger) {
	ctx := r.Context()
	remoteIP, _, _ := net.SplitHostPort(r.RemoteAddr)
	log := structlog.FromContext(ctx, nil).SetDefaultKeyvals(
		def.LogFunc, "handler",
		structlog.KeyTime, structlog.Auto,
		structlog.KeySource, structlog.Auto,
	)
	return ctx, remoteIP, log
}

func (h *Handler) healthCheck(params op.HealthCheckParams) op.HealthCheckResponder {
	return op.NewHealthCheckOK().WithPayload(&op.HealthCheckOKBody{Status: "Service is working!"})
}
