package rest

import (
	"context"
	"sync"
	"time"

	"golang.org/x/time/rate"
)

type visitor struct {
	limiter  *rate.Limiter
	lastSeen time.Time
}

type limiter struct {
	visitors     map[string]*visitor
	limitPerSec  rate.Limit
	oneTimeBurst int
	sync.Mutex
}

func newLimiter(limitPerSec float64, oneTimeBurst int) *limiter {
	l := &limiter{
		visitors:     make(map[string]*visitor),
		limitPerSec:  rate.Limit(limitPerSec),
		oneTimeBurst: oneTimeBurst,
	}

	go l.cleanupVisitors()
	return l
}

func (l *limiter) cleanupVisitors() {
	for {
		time.Sleep(time.Minute)
		l.Lock()
		for ip, v := range l.visitors {
			if time.Since(v.lastSeen) > 3*time.Minute {
				delete(l.visitors, ip)
			}
		}
		l.Unlock()
	}
}

func (l *limiter) getVisitor(ip string) *rate.Limiter {
	l.Lock()
	v, exists := l.visitors[ip]
	if !exists {
		v = &visitor{
			limiter: rate.NewLimiter(l.limitPerSec, l.oneTimeBurst),
		}
		l.visitors[ip] = v
	}
	v.lastSeen = time.Now()
	l.Unlock()
	return v.limiter
}

func (l *limiter) checkVisit(ctx context.Context, remoteIP string) error {
	lim := l.getVisitor(remoteIP)
	ctx, cancel := context.WithTimeout(ctx, 2*time.Second)
	defer cancel()
	return lim.WaitN(ctx, 1)
}
