// This file was automatically generated by genny.
// Any changes will be lost if this file is regenerated.
// see https://github.com/cheekybits/genny

package rest

import (
	"net/http"

	"gitlab.com/serkurnikov/amazon/api/openapi/restapi/op"
	"gitlab.com/serkurnikov/amazon/pkg/def"
)

func errGetFile(log Log, err error) op.GetFileResponder {
	response := errorResponse(err)
	status := int(*response.Code)

	if status < http.StatusInternalServerError {
		log.Info("client error", def.LogHTTPStatus, status, "err", err)
	} else {
		log.PrintErr("server error", def.LogHTTPStatus, status, "err", err)
	}
	return op.NewGetFileDefault(status).WithPayload(response)
}

func errUploadFile(log Log, err error) op.UploadFileResponder {
	response := errorResponse(err)
	status := int(*response.Code)

	if status < http.StatusInternalServerError {
		log.Info("client error", def.LogHTTPStatus, status, "err", err)
	} else {
		log.PrintErr("server error", def.LogHTTPStatus, status, "err", err)
	}
	return op.NewUploadFileDefault(status).WithPayload(response)
}
