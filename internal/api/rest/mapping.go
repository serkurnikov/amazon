package rest

import (
	"github.com/AlekSi/pointer"
	"gitlab.com/serkurnikov/amazon/api/openapi/model"
	"gitlab.com/serkurnikov/amazon/pkg/apierrors"
)

func mappingAPIErrorToModelError(e apierrors.APIError) *model.Error {
	return &model.Error{
		Code:        pointer.ToInt32(int32(e.Status)),
		Message:     &e.Message,
		Description: &e.Description,
	}
}
