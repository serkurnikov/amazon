package rest

import (
	"context"

	"gitlab.com/serkurnikov/amazon/api/openapi/model"
	"gitlab.com/serkurnikov/amazon/pkg/apierrors"
)

func errorResponse(err error) *model.Error {
	switch e := err.(type) {
	case apierrors.APIError:
		return mappingAPIErrorToModelError(e)
	default:
		return mappingSystemError(e)
	}
}

func mappingSystemError(err error) *model.Error {
	switch err {
	case context.DeadlineExceeded:
		return mappingAPIErrorToModelError(apierrors.Timeout)
	case context.Canceled:
		return mappingAPIErrorToModelError(apierrors.RequestTimeout)
	default:
		return mappingAPIErrorToModelError(apierrors.Internal.WithDescription(err.Error()))
	}
}
