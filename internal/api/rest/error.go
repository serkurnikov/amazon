package rest

import (
	"net/http"

	"gitlab.com/serkurnikov/amazon/api/openapi/restapi/op"
	"gitlab.com/serkurnikov/amazon/pkg/def"
)

//go:generate genny -in=$GOFILE -out=gen-$GOFILE gen "HealthCheck=GetFile,UploadFile"

func errHealthCheck(log Log, err error) op.HealthCheckResponder {
	response := errorResponse(err)
	status := int(*response.Code)

	if status < http.StatusInternalServerError {
		log.Info("client error", def.LogHTTPStatus, status, "err", err)
	} else {
		log.PrintErr("server error", def.LogHTTPStatus, status, "err", err)
	}
	return op.NewHealthCheckDefault(status).WithPayload(response)
}
