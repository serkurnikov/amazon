package api

import (
	"context"
	"net"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"syscall"

	n "github.com/nats-io/nats.go"
	"github.com/pkg/errors"
	"github.com/powerman/structlog"
	"gitlab.com/serkurnikov/amazon/api/openapi/restapi"
	"gitlab.com/serkurnikov/amazon/config"
	"gitlab.com/serkurnikov/amazon/internal/api/rest"
	"gitlab.com/serkurnikov/amazon/internal/provider/nats"
	"gitlab.com/serkurnikov/amazon/internal/provider/postgresql"
	"gitlab.com/serkurnikov/amazon/internal/services/metrics"
	"gitlab.com/serkurnikov/amazon/internal/usecase/joiner"
	"gitlab.com/serkurnikov/amazon/internal/usecase/splitter"
	"gitlab.com/serkurnikov/amazon/pkg/def"
)

func StartApiService() error {
	ctx := context.Background()

	logger := structlog.FromContext(ctx, nil)
	def.SetupLog()

	cfg := config.InitConfiguration(logger)

	runtime.GOMAXPROCS(cfg.Service.MaxCpuUsage)
	logger.Println("Current CPU Usage:", runtime.GOMAXPROCS(-1))

	metricSvc, err := metrics.New()
	if err != nil {
		logger.Fatalln("failed create metrics", err)
	}
	metricServer := metricSvc.GetServer(cfg.Prometheus.Addr())

	repo, err := postgresql.New(&cfg.Database, logger)
	if err != nil {
		logger.Fatalln("failed init repo", err)
	}

	js, err := nats.NewJetConnection(ctx, cfg.Nats, logger)
	if err != nil {
		logger.Fatalln("couldn't connect to nats", err)
	}

	eventPublishers := initEventPublishers(js, &cfg, logger)
	splitterUs := splitter.NewService(ctx, repo, &cfg, eventPublishers, logger)
	joinerUs := joiner.NewService(ctx, repo, &cfg, eventPublishers, logger)

	eventSubscriber := nats.NewSubscriber(cfg, js, logger, joinerUs)

	restServer, err := rest.NewServer(cfg.Rest, logger, metricSvc, rest.Usecases{Splitter: splitterUs, Joiner: joinerUs})
	if err != nil {
		logger.Fatalln("failed launch rest api", err)
	}

	err = GracefullyServe(
		ctx,
		logger,
		cfg,
		restServer,
		eventSubscriber,
		metricServer,
		func(ctx context.Context) error {
			_ = restServer.Shutdown()
			_ = metricServer.Shutdown(ctx)
			return nil
		},
	)
	if err != nil {
		return errors.Wrap(err, "couldn't gracefully stop application")
	}

	logger.Info("application stopped gracefully")

	return nil
}

func GracefullyServe(
	ctx context.Context,
	logger *structlog.Logger,
	cfg config.Config,
	server *restapi.Server,
	eventSubscriber *nats.Subscriber,
	metricServer *http.Server,
	teardown func(context.Context) error,
) error {
	signalChan := make(chan os.Signal, 1)
	fail := make(chan error)

	go func() {
		signal.Notify(
			signalChan,
			syscall.SIGHUP,
			syscall.SIGINT,
			syscall.SIGQUIT,
			syscall.SIGTERM,
		)

		<-signalChan
		logger.Info("Interrupting - shutting down...")

		gracefulCtx, cancelShutdown := context.WithTimeout(ctx, cfg.Service.GracePeriod)
		defer cancelShutdown()
		fail <- teardown(gracefulCtx)
	}()

	if cfg.Prometheus.Enable {
		go func() {
			err := metricServer.ListenAndServe()
			if err != nil {
				logger.Fatalln("failed launch metrics server", err)
			}
		}()
	}

	go func() {
		logger.Info("starting event subscriber")
		if err := eventSubscriber.Start(ctx); err != nil {
			logger.Fatalln("failed launch event subscriber", err)
		}
	}()

	go func() {
		for _, f := range []func() (net.Listener, error){server.HTTPListener, server.TLSListener} {
			ln, err := f()
			if err != nil {
				logger.Fatalln("listen", err)
			}
			if ln != nil {
				host, port, err := net.SplitHostPort(ln.Addr().String())
				if err != nil {
					logger.Fatalln("net.SplitHostPort", err)
				}
				logger.Println("serve", "service", "rest-api", "host", host, "port", port)
			}
		}

		err := server.Serve()
		if err != nil {
			logger.Fatalln("failed to server", err)
		}
	}()

	return <-fail
}

func initEventPublishers(js n.JetStreamContext, cfg *config.Config, logger *structlog.Logger) []*nats.Publisher {
	publishers := make([]*nats.Publisher, 0)

	for _, cluster := range cfg.Clusters {
		publishers = append(publishers, nats.NewPublisher(js, cluster.ID, logger))
	}
	return publishers
}
