package metrics

import (
	"net/http"
	"strconv"
	"time"

	"github.com/go-openapi/loads"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/serkurnikov/amazon/api/openapi/restapi"
)

type Metric struct {
	reg *prometheus.Registry

	ReqInFlight        prometheus.Gauge
	ReqTotal           *prometheus.CounterVec
	ReqDuration        *prometheus.HistogramVec
	NatsPublishedTotal *prometheus.CounterVec
}

const (
	namespace = "api"

	ResourceLabel = "resource"
	MethodLabel   = "method"
	CodeLabel     = "code"
	FailedLabel   = "failed"
	Subsystem     = "openapi"

	natsPublisher    = "nats_publisher"
	PromHandlerLabel = "handler"
)

func New() (*Metric, error) {
	var metric = &Metric{
		reg: prometheus.NewPedanticRegistry(),
	}
	metric.reg.MustRegister(prometheus.NewGoCollector()) //nolint

	metric.ReqInFlight = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace: namespace,
			Subsystem: Subsystem,
			Name:      "http_requests_in_flight",
			Help:      "Amount of currently processing API requests.",
		},
	)
	metric.reg.MustRegister(metric.ReqInFlight)

	metric.ReqTotal = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: namespace,
			Subsystem: Subsystem,
			Name:      "http_requests_total",
			Help:      "Amount of processed API requests.",
		},
		[]string{ResourceLabel, MethodLabel, CodeLabel},
	)
	metric.reg.MustRegister(metric.ReqTotal)

	metric.ReqDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: namespace,
			Subsystem: Subsystem,
			Name:      "http_request_duration_seconds",
			Help:      "API request latency distributions.",
		},
		[]string{ResourceLabel, MethodLabel, FailedLabel},
	)
	metric.reg.MustRegister(metric.ReqDuration)

	ss, err := loads.Analyzed(restapi.FlatSwaggerJSON, "")
	if err != nil {
		return nil, err
	}
	for method, resources := range ss.Analyzer.Operations() {
		for resource, op := range resources {
			codes := append([]int{}, []int{400, 401, 403, 422, 429}...)
			for code := range op.Responses.StatusCodeResponses {
				codes = append(codes, code)
			}
			for _, code := range codes {
				l := prometheus.Labels{
					ResourceLabel: resource,
					MethodLabel:   method,
					CodeLabel:     strconv.Itoa(code),
				}
				metric.ReqTotal.With(l)
			}
			for _, failed := range []string{"true", "false"} {
				l := prometheus.Labels{
					ResourceLabel: resource,
					MethodLabel:   method,
					FailedLabel:   failed,
				}
				metric.ReqDuration.With(l)
			}
		}
	}
	return metric, nil
}

func (m *Metric) GetRegistry() *prometheus.Registry {
	return m.reg
}

func (m *Metric) GetServer(addr string) *http.Server {
	handler := promhttp.InstrumentMetricHandler(m.reg, promhttp.HandlerFor(m.reg, promhttp.HandlerOpts{}))
	mux := http.NewServeMux()
	mux.Handle("/metrics", handler)
	return &http.Server{
		Addr:              addr,
		Handler:           handler,
		ReadHeaderTimeout: time.Second,
	}
}
