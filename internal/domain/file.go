package domain

type File struct {
	ID     uint32
	Name   string
	Size   int64
	Chunks []Chunk
}
