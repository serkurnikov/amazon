package domain

type Chunk struct {
	FileID         uint32
	SerialNumberID uint32
	Data           []byte
}
