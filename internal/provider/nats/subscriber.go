package nats

import (
	"context"

	"github.com/nats-io/nats.go"
	"github.com/pkg/errors"
	"github.com/powerman/structlog"
	"gitlab.com/serkurnikov/amazon/config"
	"gitlab.com/serkurnikov/amazon/internal/domain"
	"gitlab.com/serkurnikov/amazon/internal/provider/nats/internal/mw"
	"gitlab.com/serkurnikov/amazon/internal/provider/nats/internal/subscription"
	"golang.org/x/sync/errgroup"
)

type Usecases interface {
	JoinChunk(ctx context.Context, chunk domain.ChunkSaveEvent) error
}

type Subscriber struct {
	js            nats.JetStreamContext
	usecases      Usecases
	logger        *structlog.Logger
	subscriptions []*subscription.Subscription
	conf          config.Config
}

func NewSubscriber(
	conf config.Config,
	js nats.JetStreamContext,
	logger *structlog.Logger,
	usecases Usecases,
) *Subscriber {
	return &Subscriber{
		logger:   logger,
		conf:     conf,
		js:       js,
		usecases: usecases,
	}
}

func (s *Subscriber) Start(ctx context.Context) error {
	var handlers = createHandlers(s.conf)
	s.subscriptions = make([]*subscription.Subscription, 0, len(handlers))
	for _, h := range handlers {
		sub, err := subscription.NewSubscription(
			s.js,
			s.logger,
			h.stream,
			h.consumer,
			h.subject,
			h.pullInterval,
			s.conf.Nats.GracePeriod,
			h.workers,
			mw.Panic(s.logger)(mw.Timeout(h.timeout)(h.handler(s))),
		)
		if err != nil {
			return err
		}

		err = sub.Start(ctx)
		if err != nil {
			return err
		}

		s.subscriptions = append(s.subscriptions, sub)
	}

	return nil
}

func (s *Subscriber) Stop(ctx context.Context) error {
	g, _ := errgroup.WithContext(ctx)

	for _, sub := range s.subscriptions {
		s := sub
		g.Go(func() error {
			return s.Stop()
		})
	}

	if err := g.Wait(); err != nil {
		return errors.WithStack(err)
	}

	return nil
}
