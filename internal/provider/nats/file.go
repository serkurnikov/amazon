package nats

import (
	"context"
	"time"

	"github.com/nats-io/nats.go"
	"github.com/pkg/errors"
	"gitlab.com/serkurnikov/amazon/internal/domain"
	"gitlab.com/serkurnikov/amazon/internal/provider/nats/internal/subscription"
)

func chunkFileJoinHandler(s *Subscriber) subscription.Handler {
	return func(ctx context.Context, msg *nats.Msg) error {
		ctx, cancel := context.WithTimeout(context.Background(), time.Minute*10)
		defer cancel()

		var data domain.ChunkSaveEvent
		err := json.Unmarshal(msg.Data, &data)
		if err != nil {
			termErr := msg.Term()
			if termErr != nil {
				s.logger.PrintErr("couldn't term", "err", err)
			}
			return errors.Wrap(err, "couldn't unmarshal event")
		}

		err = s.usecases.JoinChunk(ctx, data)
		if err != nil {
			switch {
			default:
				if nakErr := msg.Nak(); nakErr != nil {
					s.logger.PrintErr("couldn't nak", "err", err)
				}
			}
			return err
		}

		if ackErr := msg.AckSync(); ackErr != nil {
			s.logger.PrintErr("couldn't ack", "err", err)
		}

		return nil
	}
}
