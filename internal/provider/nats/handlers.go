package nats

import (
	"time"

	"gitlab.com/serkurnikov/amazon/config"
	"gitlab.com/serkurnikov/amazon/internal/provider/nats/internal/subscription"
)

type subscribeHandler struct {
	handler      func(*Subscriber) subscription.Handler
	stream       string
	consumer     string
	subject      string
	workers      int
	pullInterval time.Duration
	timeout      time.Duration
}

func createHandlers(cfg config.Config) []subscribeHandler {
	handlers := []subscribeHandler{
		{
			stream:       "files",
			consumer:     "files-a-fetch",
			subject:      "files.a.fetch",
			workers:      cfg.Nats.SubscriptionWorkers,
			pullInterval: 50 * time.Millisecond,
			timeout:      time.Minute * 10,
			handler:      chunkFileJoinHandler,
		},
	}

	return handlers
}
