package nats

import (
	"context"

	jsoniter "github.com/json-iterator/go"
	"github.com/nats-io/nats.go"
	"github.com/pkg/errors"
	"github.com/powerman/structlog"
	"gitlab.com/serkurnikov/amazon/config"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

func NewJetConnection(
	ctx context.Context, conf config.Nats, logger *structlog.Logger,
) (nats.JetStreamContext, error) {
	opts := nats.Options{
		Url:                  conf.DSN(),
		AllowReconnect:       conf.AllowReconnect,
		MaxReconnect:         conf.MaxReconnect,
		ReconnectWait:        conf.ReconnectWait,
		Timeout:              conf.Timeout,
		PingInterval:         conf.PingInterval,
		MaxPingsOut:          conf.MaxPingsOut,
		RetryOnFailedConnect: true,
	}
	opts.DisconnectedErrCB = func(conn *nats.Conn, err error) {
		if err != nil {
			logger.PrintErr("NATS disconnect error", "err", err)
		}
	}
	opts.ReconnectedCB = func(conn *nats.Conn) {
		logger.Println("NATS reconnected")
	}
	opts.ClosedCB = func(conn *nats.Conn) {
		logger.Println("NATS connection closed")
	}

	nc, err := opts.Connect()
	if err != nil {
		return nil, errors.Wrap(err, "couldn't start connection to nats")
	}
	js, err := nc.JetStream(nats.Context(ctx))
	if err != nil {
		return nil, errors.Wrap(err, "couldn't start jetstream")
	}

	return js, nil
}
