package mw

import (
	"context"
	"time"

	"github.com/nats-io/nats.go"
	"gitlab.com/serkurnikov/amazon/internal/provider/nats/internal/subscription"
)

func Timeout(timeout time.Duration) func(h subscription.Handler) subscription.Handler {
	return func(h subscription.Handler) subscription.Handler {
		return func(ctx context.Context, msg *nats.Msg) (err error) {
			ctx, cancel := context.WithTimeout(ctx, timeout)
			defer cancel()
			return h(ctx, msg)
		}
	}
}
