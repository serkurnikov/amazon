package mw

import (
	"context"
	"runtime/debug"

	"github.com/nats-io/nats.go"
	"github.com/pkg/errors"
	"github.com/powerman/structlog"
	"gitlab.com/serkurnikov/amazon/internal/provider/nats/internal/subscription"
)

func Panic(logger *structlog.Logger) func(h subscription.Handler) subscription.Handler {
	return func(h subscription.Handler) subscription.Handler {
		return func(ctx context.Context, msg *nats.Msg) (err error) {
			defer func() {
				if r := recover(); r != nil {
					rErr, ok := r.(error)
					if !ok {
						rErr = errors.New("couldn't extract error from recover")
					}
					logger.Println("PANIC RECOVER", rErr, string(debug.Stack()))
					err = errors.New("internal service error")
				}
			}()
			return h(ctx, msg)
		}
	}
}
