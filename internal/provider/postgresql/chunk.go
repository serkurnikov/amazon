package postgresql

import (
	"context"
)

func (r *Repo) GetChunksCountByFileID(ctx context.Context, fileID uint32) (uint32, error) {
	var q = `
		select
			count(*) over() as total
			from chunks as c
			where c.file_id=$1
	`

	var total uint32
	err := r.db.QueryRowContext(ctx, q, fileID).Scan(&total)
	if err != nil {
		return 0, err
	}
	return total, nil
}
