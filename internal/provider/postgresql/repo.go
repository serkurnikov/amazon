package postgresql

import (
	"os"

	"github.com/cenkalti/backoff/v4"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/powerman/structlog"
	"gitlab.com/serkurnikov/amazon/config"

	"github.com/pressly/goose"
)

type Repo struct {
	db     *sqlx.DB
	logger *structlog.Logger
}

func New(cfg *config.Database, logger *structlog.Logger) (*Repo, error) {
	var repo = &Repo{
		logger: logger,
	}

	var err = repo.connect(cfg)
	if err != nil {
		return nil, err
	}

	err = repo.migration(cfg.MigrationPath)
	if err != nil {
		return nil, err
	}
	return repo, err
}

func (r *Repo) connect(cfg *config.Database) error {
	db, err := sqlx.Connect("postgres", cfg.DSN())
	if err != nil {
		return err
	}
	r.db = db

	r.db.SetMaxIdleConns(cfg.MaxIdleConnections)
	r.db.SetMaxOpenConns(cfg.MaxOpenConnections)

	var pingDB backoff.Operation = func() error {
		err = r.db.Ping()
		if err != nil {
			r.logger.Println("database is not ready...backing off...")
			return err
		}
		r.logger.Println("database is ready!")
		return nil
	}

	err = backoff.Retry(pingDB, backoff.NewExponentialBackOff())
	if err != nil {
		return err
	}

	r.logger.Println("database connected successful!")
	return nil
}

func (r *Repo) migration(migrationPath string) error {
	var err error

	err = goose.SetDialect("postgres")
	if err != nil {
		return err
	}

	current, err := goose.EnsureDBVersion(r.db.DB)
	if err != nil {
		return err
	}
	files, err := os.ReadDir(migrationPath)
	if err != nil {
		return err
	}

	migrations, err := goose.CollectMigrations(migrationPath, current, int64(len(files)))
	if err != nil {
		return err
	}

	for _, m := range migrations {
		if err := m.Up(r.db.DB); err != nil {
			return err
		}
	}
	return nil
}

func (r *Repo) Disconnect() error {
	r.logger.Println("pq database will be closed...")
	return r.db.Close()
}
