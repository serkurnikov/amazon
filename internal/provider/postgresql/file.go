package postgresql

import (
	"context"

	"gitlab.com/serkurnikov/amazon/internal/domain"
)

func (r *Repo) CreateFile(ctx context.Context, f *domain.File) (uint32, error) {
	var q = `
		insert into files(name, size)
		values ($1, $2)
		returning id
	`
	var id uint32
	var err = r.db.QueryRowContext(ctx, q, f.Name, f.Size).Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, err
}

func (r *Repo) UpdateFileChunksCount(ctx context.Context, fileID, count uint32) error {
	q := `
		update files set chunks=$1, updated_at = now() at time zone 'UTC' where id=$2;
	`
	_, err := r.db.ExecContext(ctx, q, count, fileID)
	return err
}

func (r *Repo) GetFileNameByID(ctx context.Context, fileID uint32) (string, error) {
	var fileName string
	q := `
        select name FROM files WHERE id = $1
    `
	err := r.db.QueryRowContext(ctx, q, fileID).Scan(&fileName)
	if err != nil {
		return "", err
	}
	return fileName, nil
}
