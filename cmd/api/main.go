package main

import (
	"os"

	"gitlab.com/serkurnikov/amazon/internal/api"
)

func main() {
	var err = api.StartApiService()
	if err != nil {
		os.Exit(1)
	}
	os.Exit(0)
}
