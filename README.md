# Amazon - powerful storage

**Table of Contents**

- [Overview](#overview)
    - [The Clean Architecture](#the-clean-architecture)
    - [The hexagonal architecture, or ports and adapters architecture](#the-hexagonal-architecture-or-ports-and-adapters-architecture)
    - [Structure of Go packages](#structure-of-go-packages)
    - [Features](#features)
- [Run](#run)
    - [Locally](#locally)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Overview
### The Clean Architecture

[![The Clean Architecture](https://koenig-media.raywenderlich.com/uploads/2019/02/Clean-Architecture-Bob.png)]()

It's not a complete example of The Clean Architecture itself
("Use Cases" layer in package `app` embeds "Entities" layer), but it does show the most relevant part: how to create "
API Controller" layer in package `srv/openapi`
between code auto-generated by go-swagger and "Use Cases" layer in package
`app`. Also, it includes "DB Gateway" layer in packages `dal/repo` (provided trivial in-memory implementation is "DB"
and "Gateway" layers at once) and
`dal/sql` (just "Gateway" layer).

### The hexagonal architecture, or ports and adapters architecture

It may be even easier to understand implemented architecture as "ports and adapters":

- "ports" are defined as interfaces in `app/app.go` - they make it possible to easily test business-logic in `app`
  without any external dependencies by mocking all these interfaces.
- "adapters" are implemented in `srv/*` (serve project APIs), `dal/*`
  (access DB) and `svc/*` (use external services) packages - they can't be tested that easy, so they should be as thin
  and straightforward as possible and try hard to do nothing than convert ("adapt") data between format used by external
  world and our business-logic (package `app`).

### Structure of Go packages

- `api/*` - definitions of own and 3rd-party APIs/protocols and related auto-generated code
- `cmd/*` - main application(s)
- `config/*` - configuration(s) (default values, env, flags) for application(s) subcommands and tests
- `internal/api` - define server with configurations
    - `internal/api/rest` - handlers implementations
- `internal/domain/*` - domain structures (for NATs streaming, postgresSQL)
- `internal/provider/*` - adapters for NATs & postgresSQL communication
- `internal/services/*` - external usefully services
- `internal/usecase/*` - splitter & joiner usecase. 
- `pkg/*` - helper packages, not related to architecture and business-logic (may be later moved to own modules and/or
  replaced by external dependencies), e.g.:
    - `pkg/def/` - project-wide defaults

### Application Architecture Overview
The application follows a microservices architecture and leverages two key components for data storage and communication: NATS for chunk storage and TimescaleDB for data storage.
![scheme.png](assets%2Fscheme.png)

#### NATS for Chunk Storage:

NATS is used as a messaging system to facilitate communication between different parts of the application. In this architecture, NATS plays a crucial role in handling the storage of file chunks. Here's how it works:

Publishers: Multiple clusters or publishers are responsible for sending and receiving data chunks. These clusters distribute the load and ensure high availability.
Chunk Events: When a file needs to be stored, it is split into smaller data chunks. Each chunk is then sent as a "Chunk Event" to the corresponding NATS subject, which corresponds to a specific cluster or publisher.
Data Distribution: NATS ensures that these data chunks are evenly distributed among the available clusters, preventing any single cluster from being overwhelmed with data.
Acknowledgment: An "acknowledgment" policy is used, ensuring that data chunks are only removed from the NATS subject when they have been successfully received by the intended cluster.

#### TimescaleDB for Data Storage:

TimescaleDB is chosen as the primary data storage solution for the application. It is a time-series database that excels at handling large volumes of time-stamped data efficiently. Here's how it fits into the architecture:

File and Chunk Metadata: TimescaleDB stores metadata related to files, such as file names and sizes. It also stores metadata related to individual data chunks, including file associations, serial numbers, and unique identifiers.
Query and Retrieval: TimescaleDB provides efficient querying capabilities, making it easy to retrieve specific files and their associated chunks based on various criteria, such as file IDs, time stamps, or other relevant attributes.
High Performance: TimescaleDB's performance characteristics, especially in handling time-series data, make it a suitable choice for storing and retrieving file-related metadata efficiently.
Benefits of the Architecture:

Scalability: The architecture allows for horizontal scalability by adding more clusters or publishers as needed to handle increased data storage and processing requirements.
Fault Tolerance: The use of NATS ensures that data chunks are distributed among multiple clusters, increasing fault tolerance and availability.
Efficient Data Retrieval: TimescaleDB's query optimization features make it efficient for retrieving specific file data or metadata, which is crucial for serving application requests.
Modular Design: The application's modular design separates concerns related to chunk storage and metadata storage, making it easier to maintain and scale each component independently.
Overall, this architecture leverages the strengths of NATS for efficient chunk distribution and TimescaleDB for storing and retrieving file metadata, ensuring a reliable and high-performance system for handling large volumes of data.
### Features

- [X] Project structure (mostly) follow [Standard Go Project Layout](https://github.com/golang-standards/project-layout)
  .
- [X] Strict but convenient golangci-lint configuration.
- [X] Easily testable code (thanks to The Clean Architecture).
- [X] Avoids (and resists to) using global objects (to make it possible to embed such microservices into modular
  monolith).
- [X] Graceful shutdown support.
- [X] Configuration defaults can be overwritten by `config.uaml` and flags.
- [X] CORS support, so you can play with API using Swagger Editor tool.
- [X] Production logging using [structlog](https://github.com/powerman/structlog).
- [X] Production metrics using Prometheus.
- [X] Docker and docker-compose support.
- [X] Smart test coverage report
- [X] Linters for Dockerfile and shell scripts.
- [X] CI/CD setup for GitLab Actions and CircleCI.

## Run
### Locally
You can use docker-compose 
```bash
docker-compose build
docker-compose up  
```

For stopping 
```bash
docker-compose down
```

Also, you need to create NATs streaming 

```bash
make create-streams
make create-consumers
```
Check that all streams and consumers are ready
```bash
nats stream ls
nats consumer ls
```

- And start the server
```bash
 INF:  Oct  5 18:56:40 server.go:35 @ api.StartApiService `Current CPU Usage: 4`
 INF:  Oct  5 18:56:40 repo.go:53 @ postgresql.(*Repo).connect.func1 `database is ready!`
 INF:  Oct  5 18:56:40 repo.go:62 @ postgresql.(*Repo).connect `database connected successful!`
 INF:  Oct  5 18:56:40 server.go:125 @ api.GracefullyServe.func3 `starting event subscriber`
 INF:  Oct  5 18:56:40 server.go:142 @ api.GracefullyServe.func4 `serve service rest-api host 127.0.0.1 port 8080`
 INF:  Oct  5 18:56:40 server.go:109 @ restapi.(*Server).Logf `Serving amazon API at http://127.0.0.1:8080`
```

More information about [NATS Docs](https://docs.nats.io/running-a-nats-service/nats_docker)

- Check service
```bash
curl --request GET \
--url http://127.0.0.1:8080/api/v1/health-check
```

```bash
{
	"status": "Service is working!"
}
```

### Example of requests
Upload file
```bash
curl --request POST \
  --url http://127.0.0.1:8080/api/v1/upload \
  --header 'Content-Type: multipart/form-data' \
  --form file=@/Users/sergey/Downloads/20230812_181140.jpg
```

response 
```bash
{
"id": 22
}
```

Fetch file (you need using fileID from response)
```bash
curl --request GET \
  --url 'http://127.0.0.1:8080/api/v1/get?id=22'
```

```bash
{
"content": "(base64)->iVBORw0KGgoA.....",
"id": 23,
"name": "20230812_181140.jpg"
}
```