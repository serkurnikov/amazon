-- +goose Up
create schema if not exists public;

create table if not exists public.files
(
    id         serial primary key,
    name       text    not null,
    size       integer not null,
    chunks     integer not null default 1,
    created_at timestamp without time zone default (now() at time zone 'utc'),
    updated_at timestamp without time zone
);

create table if not exists public.chunks
(
    id               serial primary key,
    file_id          integer not null references files (id)
        on update cascade
        on delete cascade,
    serial_number_id integer not null,
    uuid             text    not null,
    cluster_id       integer not null,
    created_at       timestamp without time zone default (now() at time zone 'utc'),
    updated_at       timestamp without time zone
);
create unique index if not exists file_serial_number_idx on chunks (file_id, serial_number_id);
create unique index if not exists uuid_idx on chunks (uuid);
-- +goose Down
-- sql in this section is executed when the migration is rolled back.
